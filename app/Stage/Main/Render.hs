module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui

import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.Render.Dear qualified as Dear

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} =
  Scene.observe rsSceneP frScene

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb _fr imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass}) <- ask -- TODO: pass frame to usePass

  dear <- Dear.imguiDrawData

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    ImGui.draw dear cb
