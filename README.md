# Keid - Positional sound


## Using HRTF

You can force spatial audio processing with OpenAL-Soft in `~/.alsoftrc`:

```ini
[General]
stereo-mode = headphones
hrtf = true
hrtf-mode = full
resampler = bsinc24
front-stablizer = true
```
